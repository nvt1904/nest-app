import { Module } from '@nestjs/common';
import { DBService } from '@database/db.service';
import { CloudinaryController } from './cloudinary.controller';
import { CloudinaryService } from './cloudinary.service';

@Module({
  controllers: [CloudinaryController],
  providers: [DBService, CloudinaryService]
})
export class CloudinaryModule {}
