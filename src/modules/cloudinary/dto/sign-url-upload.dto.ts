import { ApiProperty } from '@nestjs/swagger';

export class SignUrlUploadRes {
  @ApiProperty()
  public_id: string;

  @ApiProperty()
  timestamp: number;

  @ApiProperty()
  signature: string;

  @ApiProperty()
  api_key: string;
}
