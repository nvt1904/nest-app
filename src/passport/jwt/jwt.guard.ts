import Unauthorized from '@exceptions/Unauthorized';
import { ExecutionContext, Injectable } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { AuthGuard } from '@nestjs/passport';
import { AUTH_JWT_ROLES_KEY } from './jwt.decorator';

@Injectable()
export class AuthJwtGuard extends AuthGuard('jwt') {
  private roles: undefined | string[];

  constructor(private reflector: Reflector) {
    super();
  }

  canActivate(context: ExecutionContext) {
    // Add your custom authentication logic here
    // for example, call super.logIn(request) to establish a session.
    this.roles = this.reflector.get<string[]>(
      AUTH_JWT_ROLES_KEY,
      context.getHandler()
    );
    if (this.roles) {
      return super.canActivate(context);
    }
    return true;
  }

  handleRequest(err, user) {
    // You can throw an exception based on either "info" or "err" arguments
    if (
      err ||
      !user ||
      (this.roles?.length && !this.roles.includes(user.type))
    ) {
      throw err || new Unauthorized();
    }
    return user;
  }
}
