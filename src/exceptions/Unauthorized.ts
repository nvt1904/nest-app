import { HttpStatus } from '@nestjs/common';
import { ApiException, Extensions } from '.';

export default class Unauthorized extends ApiException {
  public code = 'unauthorized';
  public constructor(message?: string, extensions?: Extensions) {
    super(HttpStatus.UNAUTHORIZED, message, extensions);
  }
}
