import { Controller, Get, Query } from '@nestjs/common';
import { ApiBadRequestResponse, ApiOkResponse, ApiTags } from '@nestjs/swagger';
import { ApiException } from '@exceptions';
import { CloudinaryService } from './cloudinary.service';
import { SignUrlUploadRes } from './dto/sign-url-upload.dto';

@Controller('cloudinary')
@ApiTags('Cloudinary')
export class CloudinaryController {
  constructor(private cloudinaryService: CloudinaryService) {}

  @Get('sign-url-upload')
  @ApiOkResponse({ type: SignUrlUploadRes })
  @ApiBadRequestResponse({ type: ApiException })
  signUrlUpload(@Query('name') name?: string) {
    return this.cloudinaryService.signUrlUpload(name);
  }
}
