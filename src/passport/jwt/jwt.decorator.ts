import Unauthorized from '@exceptions/Unauthorized';
import {
  createParamDecorator,
  ExecutionContext,
  SetMetadata
} from '@nestjs/common';

export type Payload = {
  id: string;
  type: 'user';
};

export const AUTH_JWT_ROLES_KEY = 'AUTH_JWT_ROLES';

export const AuthJwtRoles = (...roles: string[]) =>
  SetMetadata(AUTH_JWT_ROLES_KEY, roles);

export const ReqAuth = createParamDecorator<boolean, ExecutionContext, Payload>(
  (exception = false, ctx: ExecutionContext) => {
    const request = ctx.switchToHttp().getRequest();
    if (exception && !request.user?.id) {
      throw new Unauthorized();
    }
    return request.user as Payload;
  }
);
