import { ApiProperty } from '@nestjs/swagger';

export class LoginAuthDto {
  @ApiProperty()
  email: string;

  @ApiProperty()
  password: string;
}

export class LoginAuthRes {
  @ApiProperty()
  accessToken: string;

  @ApiProperty()
  expires: string;

  @ApiProperty()
  refreshToken: string;
}
