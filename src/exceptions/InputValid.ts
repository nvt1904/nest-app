import { HttpStatus } from '@nestjs/common';
import { ApiException, Extensions } from '.';

export default class InputValid extends ApiException {
  public code = 'input_valid';
  public constructor(message?: string, extensions?: Extensions) {
    super(HttpStatus.BAD_REQUEST, message, extensions);
  }
}
