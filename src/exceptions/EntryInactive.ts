import { HttpStatus } from '@nestjs/common';
import { ApiException, Extensions } from '.';

export default class EntryInactive extends ApiException {
  public code = 'entry_inactive';
  public constructor(message?: string, extensions?: Extensions) {
    super(HttpStatus.BAD_REQUEST, message, extensions);
  }
}
