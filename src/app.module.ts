import { CloudinaryModule } from '@/modules/cloudinary/cloudinary.module';
import { ApiExceptionFilter } from '@exceptions';
import { AuthModule } from '@modules/auth/auth.module';
import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { APP_FILTER, APP_GUARD } from '@nestjs/core';
import { AuthJwtGuard } from '@passport/jwt/jwt.guard';
import { JwtStrategy } from '@passport/jwt/jwt.strategy';
import {
  AcceptLanguageResolver,
  CookieResolver,
  HeaderResolver,
  I18nJsonParser,
  I18nModule,
  QueryResolver
} from 'nestjs-i18n';
import * as path from 'path';
import { HealthModule } from './modules/health/health.module';
@Module({
  imports: [
    ConfigModule,
    HealthModule,
    I18nModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: async (configService: ConfigService) => ({
        fallbackLanguage: configService.get<string>('DEFAULT_LANGUAGE', 'vi'),
        parserOptions: {
          path: path.join(__dirname, '/i18n/')
        }
      }),
      parser: I18nJsonParser,
      inject: [ConfigService],
      resolvers: [
        { use: QueryResolver, options: ['lang', 'locale'] },
        new HeaderResolver(['x-custom-lang']),
        AcceptLanguageResolver,
        new CookieResolver(['lang', 'locale'])
      ]
    }),
    AuthModule,
    CloudinaryModule
  ],
  providers: [
    {
      provide: APP_GUARD,
      useClass: AuthJwtGuard
    },
    {
      provide: APP_FILTER,
      useClass: ApiExceptionFilter
    },
    JwtStrategy
  ]
})
export class AppModule {}
