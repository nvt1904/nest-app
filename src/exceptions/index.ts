import {
  ExceptionFilter,
  Catch,
  ArgumentsHost,
  HttpException,
  HttpStatus,
  Logger
} from '@nestjs/common';
import { ApiProperty } from '@nestjs/swagger';
import { Response } from 'express';
import { I18nService } from 'nestjs-i18n';

export type Extensions = Record<string, unknown>;

export class ApiException extends HttpException {
  @ApiProperty({ type: 'string' })
  public code = 'error';

  @ApiProperty()
  public message: string;

  @ApiProperty()
  public extensions: Extensions;

  public constructor(
    status: HttpStatus,
    message?: string,
    extensions: Extensions = {}
  ) {
    super(message, status);
    this.extensions = extensions;
  }
}

@Catch(ApiException)
export class ApiExceptionFilter implements ExceptionFilter {
  constructor(private readonly i18n: I18nService) {}
  async catch(exception: ApiException, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const req = ctx.getRequest<Request>();
    const res = ctx.getResponse<Response>();
    const status = exception.getStatus();
    const message = await this.i18n.translate(`error.${exception.code}`, {
      lang: ctx.getRequest().i18nLang
    });
    Logger.error(
      ` ${req.method} ${req.url} ${JSON.stringify(req.body)} ${JSON.stringify(
        message
      )} ${exception.stack}`
    );
    res.status(status).json({
      error: exception.code,
      message,
      extensions: exception.extensions
    });
  }
}
