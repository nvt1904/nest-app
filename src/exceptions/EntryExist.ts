import { HttpStatus } from '@nestjs/common';
import { ApiException, Extensions } from '.';

export default class EntryExist extends ApiException {
  public code = 'entry_exist';
  public constructor(message?: string, extensions?: Extensions) {
    super(HttpStatus.BAD_REQUEST, message, extensions);
  }
}
