import { Payload } from '@passport/jwt/jwt.decorator';
import { DBService } from '@database/db.service';
import ArgumentError from '@exceptions/ArgumentError';
import EntryExist from '@exceptions/EntryExist';
import EntryInactive from '@exceptions/EntryInactive';
import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { JwtService } from '@nestjs/jwt';
import { UserStatus } from '@prisma/client';
import * as bcrypt from 'bcrypt';
import { addSeconds } from 'date-fns';
import { LoginAuthDto } from './dto/login-auth.dto';
import { RegisterAuthDto } from './dto/register-auth.dto';

@Injectable()
export class AuthService {
  constructor(
    private readonly dbService: DBService,
    private readonly jwtService: JwtService,
    private readonly configService: ConfigService
  ) {}

  async register(registerAuthDto: RegisterAuthDto) {
    const { name, email, password } = registerAuthDto;
    let user = await this.dbService.user.findFirst({
      where: { email },
      select: { id: true, password: true, status: true }
    });
    if (user) {
      throw new EntryExist();
    }
    const passwordHash = bcrypt.hashSync(password, 10);
    user = await this.dbService.user.create({
      data: {
        name,
        email,
        password: passwordHash
      }
    });
    return this.login({ email, password });
  }

  async login(loginAuthDto: LoginAuthDto) {
    const { email, password } = loginAuthDto;
    const user = await this.dbService.user.findFirst({
      where: { email },
      select: { id: true, password: true, status: true }
    });
    if (user && user.status === UserStatus.active) {
      if (bcrypt.compareSync(password, user?.password)) {
        const payload: Payload = { id: user.id, type: 'user' };
        return {
          accessToken: this.jwtService.sign(payload),
          refreshToken: this.jwtService.sign(payload, {
            expiresIn: this.configService.get<number>(
              'JWT_ACCESS_TOKEN_EXPIRES'
            )
          }),
          expires: addSeconds(
            new Date(),
            this.configService.get<number>('JWT_ACCESS_TOKEN_EXPIRES')
          )
        };
      }
      throw new ArgumentError();
    }
    throw new EntryInactive();
  }

  async profile(id: string) {
    return await this.dbService.user.findFirst({
      where: { id: id },
      select: {
        id: true,
        avatar: true,
        name: true,
        email: true,
        status: true
      }
    });
  }
}
