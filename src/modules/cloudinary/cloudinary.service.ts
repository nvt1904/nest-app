import { Injectable } from '@nestjs/common';
import { format } from 'date-fns';
import * as slug from 'slug';
import * as cloudinary from 'cloudinary';
import { strGenerate } from '@shared/helper';

@Injectable()
export class CloudinaryService {
  signUrlUpload(name?: string) {
    const fileName = `${format(new Date(), 'yyyyMMddHHmmss')}-${slug(
      name || strGenerate({ length: 10, lowerCase: true })
    )}`;
    return cloudinary.v2.utils.sign_request({
      public_id: `temp/${fileName}`,
      timestamp: Math.round(new Date().getTime() / 1000)
    });
  }
}
