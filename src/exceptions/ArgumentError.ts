import { HttpStatus } from '@nestjs/common';
import { ApiException, Extensions } from '.';

export default class ArgumentError extends ApiException {
  public code = 'argument_error';
  public constructor(message?: string, extensions?: Extensions) {
    super(HttpStatus.BAD_REQUEST, message, extensions);
  }
}
