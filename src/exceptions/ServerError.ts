import { HttpStatus } from '@nestjs/common';
import { ApiException, Extensions } from '.';

export default class ServerError extends ApiException {
  public code = 'server_error';
  public constructor(message?: string, extensions?: Extensions) {
    super(HttpStatus.SERVICE_UNAVAILABLE, message, extensions);
  }
}
