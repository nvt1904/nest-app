import { AuthJwtRoles, Payload, ReqAuth } from '@passport/jwt/jwt.decorator';
import { ApiException } from '@exceptions';
import { Body, Controller, Get, Post } from '@nestjs/common';
import {
  ApiBadRequestResponse,
  ApiBearerAuth,
  ApiOkResponse,
  ApiTags
} from '@nestjs/swagger';
import { AuthService } from './auth.service';
import { LoginAuthDto, LoginAuthRes } from './dto/login-auth.dto';
import { RegisterAuthDto } from './dto/register-auth.dto';
@Controller('auth')
@ApiTags('Auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Post('register')
  @ApiOkResponse({ type: LoginAuthRes })
  @ApiBadRequestResponse({ type: ApiException })
  register(@Body() registerAuthDto: RegisterAuthDto) {
    return this.authService.register(registerAuthDto);
  }

  @Post('login')
  @ApiOkResponse({ type: LoginAuthRes })
  @ApiBadRequestResponse({ type: ApiException })
  login(@Body() loginAuthDto: LoginAuthDto) {
    return this.authService.login(loginAuthDto);
  }

  @Get('profile')
  @ApiBearerAuth()
  @ApiOkResponse({ type: LoginAuthRes })
  @ApiBadRequestResponse({ type: ApiException })
  @AuthJwtRoles()
  profile(@ReqAuth() user: Payload) {
    return this.authService.profile(user.id);
  }
}
