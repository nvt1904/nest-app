export default {
  info: {
    name: process.env.INFO_NAME || 'Nest App',
    description: process.env.INFO_DESCRIPTION || 'Nest App',
    version: process.env.INFO_VERSION || '1.0'
  },
  jwt: {
    secretKey: process.env.JWT_SECRET_KEY || 'nest-app',
    accessTokenExpires: Number(process.env.JWT_ACCESS_TOKEN_EXPIRES) || 60 * 10,
    refreshTokenExpires:
      Number(process.env.JWT_REFRESH_TOKEN_EXPIRES) || 60 * 60 * 24 * 30
  },
  redis: {
    url: process.env.REDIS_URL || 'redis://localhost:6379'
  },
  cache: {
    ttlDefault: Number(process.env.CACHE_TTL_DEFAULT || 3600)
  }
};
