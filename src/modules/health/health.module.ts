import { DBService } from '@database/db.service';
import { HttpModule } from '@nestjs/axios';
import { Module } from '@nestjs/common';
import { TerminusModule } from '@nestjs/terminus';
import { HealthConsumer } from './health.consumer';
import { HealthController } from './health.controller';

@Module({
  imports: [TerminusModule, HttpModule],
  providers: [DBService, HealthConsumer],
  controllers: [HealthController]
})
export class HealthModule {}
